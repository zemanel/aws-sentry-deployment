module github.com/zemanel/aws-sentry-deployment

go 1.13

require (
	github.com/gruntwork-io/terratest v0.26.6
	github.com/stretchr/testify v1.5.1 // indirect
)
