resource "aws_vpc" "main" {
  cidr_block         = local.cidr_block
  enable_dns_support = local.enable_dns_support

  tags = merge({
    Name = local.name

  }, local.tags)
}
