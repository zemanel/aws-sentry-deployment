locals {
  name               = var.name
  cidr_block         = var.cidr_block
  enable_dns_support = var.enable_dns_support
  tags               = var.tags
}
