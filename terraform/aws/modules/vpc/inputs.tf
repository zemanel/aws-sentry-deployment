variable "name" {
  description = "VPC Name"
}

variable "cidr_block" {
  description = "VPC CIDR block"
}

variable "enable_dns_support" {
  description = "A boolean flag to enable/disable DNS support in the VPC."
  default     = true
}

variable "tags" {
  description = "A mapping of tags to assign to the resource."
}
