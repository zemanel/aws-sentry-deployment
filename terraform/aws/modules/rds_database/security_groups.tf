resource "aws_security_group" "allow_postgres" {
  name        = "allow_postgres"
  description = "Allow Postgres inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "Allow Postgres inbound traffic from whitelisted networks"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = var.allowed_cidr_blocks
  }

  tags = merge({
    Name = "allow_postgres"
  }, var.tags)
}
