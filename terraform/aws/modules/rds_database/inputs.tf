variable "vpc_id" {
  description = "VPC ID"
}

variable "identifier" {
  description = "Database instance identifier"
}

variable "engine" {
  description = "Database Engine"
  default     = "postgres"
}

variable "engine_version" {
  description = "Database Version"
  default     = "9.6.9"
}

variable "instance_class" {
  description = "EC2 instance class"
  default     = "db.t2.small"
}

variable "storage_encrypted" {
  description = "Encrypt EBS storage or not"
  default     = true
}

variable "allocated_storage" {
  description = "Database EBS storage size (GB)"
  default     = 5
}

variable "name" {
  description = "Database name"
}

variable "username" {
  description = "Database master username"
}

variable "password" {
  description = "Database master password"
}

variable "port" {
  description = "Database port"
  default     = "5432"
}

variable "maintenance_window" {
  description = "Maintenace window"
  default     = "Mon:00:00-Mon:03:00"
}

variable "backup_window" {
  description = "Maintenace window for backups"
  default     = "03:00-06:00"
}

variable "backup_retention_period" {
  description = "A mapping of tags to assign to the resource."
  default     = 0
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to enable for exporting to CloudWatch logs."
  type        = list(string)
  default     = ["postgresql", "upgrade"]
}

variable "family" {
  description = "Database family"
  default     = "postgres9.6"
}

variable "major_engine_version" {
  description = "Database engine version"
  default     = "9.6"
}

variable "deletion_protection" {
  description = "Database Deletion Protection"
  default     = false
}

variable "final_snapshot_identifier" {
  description = "final snapshot identifier"
  type        = string
  default     = "final"
}

variable "tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map
  default     = {}
}

variable "allowed_cidr_blocks" {
  description = "A mapping of tags to assign to the resource."
  type        = list(string)
  default     = []
}

variable "subnet_cidr_blocks" {
  description = "CIDR block for the RDS subnet"
  type        = list(string)
}

variable "subnet_availability_zones" {
  description = "The AZ for the RDS subnet"
  type        = list(string)
}
