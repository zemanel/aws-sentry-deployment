resource "aws_subnet" "rds" {
  count = length(var.subnet_cidr_blocks)

  vpc_id            = var.vpc_id
  cidr_block        = element(var.subnet_cidr_blocks, count.index)
  availability_zone = element(var.subnet_availability_zones, count.index)

  tags = merge({
    Name = "allow_postgres"
  }, var.tags)
}
