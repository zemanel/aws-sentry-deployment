resource "aws_kms_key" "rds" {
  description             = "RDS storage encryption"
  deletion_window_in_days = 10
  tags                    = var.tags
}
