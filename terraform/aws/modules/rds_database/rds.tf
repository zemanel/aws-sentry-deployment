module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "2.14.0"

  identifier = var.identifier

  engine         = var.engine
  engine_version = var.engine_version
  instance_class = var.instance_class

  allocated_storage = var.allocated_storage
  storage_encrypted = var.storage_encrypted
  kms_key_id        = aws_kms_key.rds.arn

  name     = var.name
  username = var.username
  password = var.password
  port     = var.port

  vpc_security_group_ids = [aws_security_group.allow_postgres.id]

  maintenance_window = var.maintenance_window
  backup_window      = var.backup_window

  # disable backups to create DB faster
  backup_retention_period = var.backup_retention_period

  tags = local.tags

  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports

  # DB subnet group
  subnet_ids = aws_subnet.rds.*.id

  # DB parameter group
  family = var.family

  # DB option group
  major_engine_version = var.major_engine_version

  # Snapshot name upon DB deletion
  final_snapshot_identifier = var.final_snapshot_identifier

  # Database Deletion Protection
  deletion_protection = var.deletion_protection
}
