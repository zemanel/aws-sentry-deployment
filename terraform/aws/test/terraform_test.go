package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
)

// Test_TerraformInfrastructure tests creating Terraform infrastructure
func Test_TerraformInfrastructure(t *testing.T) {
	terraformOptions := &terraform.Options{
		TerraformDir: ".",
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)
}
