# Test Infrastructure


// random id for concurrent test pipelines
resource "random_id" "suffix" {
  byte_length = 8
}

module "vpc" {
  source     = "../modules/vpc"
  name       = "test-${random_id.suffix.hex}"
  cidr_block = "10.1.0.0/16"

  tags = {
    Environment = "test"
  }
}

module "database" {
  source = "../modules/rds_database"

  vpc_id = module.vpc.vpc_id

  name     = "test${random_id.suffix.hex}"
  username = "demouser"
  password = "YourPwdShouldBeLongAndSecure!"
  port     = "5432"

  identifier          = "demodb"
  allowed_cidr_blocks = []

  # subnets and related AZ must cover at least 2 availability zones.
  # matched by array indexes (map construct can be utilised here instead).
  subnet_availability_zones = ["eu-west-1a", "eu-west-1b"]
  subnet_cidr_blocks        = ["10.1.1.0/24", "10.1.2.0/24"]

  tags = {
    Environment = "test"
  }
}
