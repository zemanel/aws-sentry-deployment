module "vpc" {
  source             = "../modules/vpc"
  name               = "main"
  cidr_block         = "10.0.0.0/16"
  enable_dns_support = true
  tags = {
    Environment = "production"
  }
}
