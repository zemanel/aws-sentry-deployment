.PHONY: test

# Run funcional tests
test:
	go test -count=1 -v ./... -timeout 30m

